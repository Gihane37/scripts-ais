

@echo off

:REM check if "HKLM\Software\Product Labs" registry key is present
reg query "HKLM\Software\Product Labs"

:REM if "HKLM\Software\Product Labs" registry key is present, it means that sentinel has already been installed on this host, so go to the INSTALLED switch of the script
IF %ERRORLEVEL% == 0 goto INSTALLED

:REM Copy ProductInstaller_windows.msi installer from SYSVOL share to local TEMP folder WORKSTATION
copy \\ad.domain.fr\SYSVOL\ad.domain.fr\scripts\productInstaller.msi c:\windows\temp\ /Z /Y

:REM install msi package
msiexec /i "c:\windows\temp\ProductInstaller.msi" /q /norestart SITE_TOKEN="suite de caratères=="

:REM if install is ok go to OK switch
IF %ERRORLEVEL% == 0 goto OK

:REM if install fails go to ERROR switch
goto ERROR

:INSTALLED
echo "Already Installed"
goto END

:ERROR
echo "Install Error"
goto END

:OK
echo "Install OK"

:END

